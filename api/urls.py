from django.conf.urls import url
from . import views
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    url(r'^login$', obtain_auth_token),
    url(r'^profile$', views.GamerProfileView.as_view()),

    url(r'^achievements/all$', views.AllAchievementsView.as_view()),
    url(r'^achievements/my$', views.MyAchievementsView.as_view()),

    url(r'^countries$', views.CountriesView.as_view()),
    url(r'^cities$', views.CitiesView.as_view()),
    url(r'^airports$', views.AirportsView.as_view()),

    url(r'^adventures/all$', views.AllAdventuresView.as_view()),
    url(r'^adventures/my$', views.MyAdventuresView.as_view()),
    url(r'^adventure/(?P<pk>\d+)/tasks$', views.AdventureTasksView.as_view()),
    url(r'^adventure/(?P<pk>\d+)/apply$', views.ApplyToAdventureView.as_view()),
]
