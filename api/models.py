from django.conf import settings
from django.dispatch import receiver
from django.db import models
from django.db.models import SET_NULL, CASCADE, Sum
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


class Achievement(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(upload_to='uploads/achievements')
    count = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Adventure(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(upload_to='uploads/adventures')
    achievement = models.ForeignKey(
        Achievement,
        null=True,
        blank=True,
        default=None,
        on_delete=SET_NULL
    )
    reward = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Task(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(upload_to='uploads/tasks')
    adventure = models.ForeignKey(Adventure, on_delete=CASCADE,
                                  related_name='tasks')
    achievement = models.ForeignKey(Achievement, null=True, blank=True,
                                    on_delete=SET_NULL)
    orig = models.ForeignKey('Airport', null=True, default=True,
                             on_delete=CASCADE, related_name='task_from')
    dest = models.ForeignKey('Airport', null=True, default=True,
                             on_delete=CASCADE, related_name='task_to')

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=255)
    key = models.CharField(max_length=2)

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=255)
    country = models.ForeignKey(Country, null=True, default=None,
                                on_delete=CASCADE)

    def __str__(self):
        return self.name


class Airport(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=3)

    city = models.ForeignKey(City, null=True, default=None, on_delete=CASCADE)
    lat = models.FloatField()
    lon = models.FloatField()

    def __str__(self):
        return self.name


class Aircraft(models.Model):
    name = models.CharField(max_length=255)


class GamerProfile(models.Model):
    user = models.OneToOneField(User, null=True, related_name='gamer')
    achievements = models.ManyToManyField(Achievement, through='AchievementLog')
    adventures = models.ManyToManyField(Adventure, through='AdventureProgress')
    tasks = models.ManyToManyField(Task, through='AdventureProgress')

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def score(self):
        return ScoreLog.objects.filter(gamer=self) \
            .aggregate(score=Sum('amount'))['score']


class AchievementLog(models.Model):
    gamer = models.ForeignKey(GamerProfile, on_delete=CASCADE)
    achievement = models.ForeignKey(Achievement, on_delete=CASCADE)
    timestamp = models.DateTimeField(auto_now=True)
    progress = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.achievement} / {self.gamer} / {self.progress}'

    @property
    def completed(self):
        return self.progress >= self.achievement.count


class ScoreLog(models.Model):
    gamer = models.ForeignKey(GamerProfile, on_delete=CASCADE)
    amount = models.IntegerField()

    def __str__(self):
        return f'{self.gamer} / {self.amount}'


class AdventureProgress(models.Model):
    gamer = models.ForeignKey(GamerProfile, null=True, default=None,
                              on_delete=CASCADE)
    adventure = models.ForeignKey(Adventure, null=True, default=None,
                                  on_delete=CASCADE)
    task = models.ForeignKey(Task, null=True, default=None, on_delete=SET_NULL)

    def __str__(self):
        return f'{self.gamer} / {self.adventure} / {self.task}'


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_related_models(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        GamerProfile.objects.create(user=instance)

