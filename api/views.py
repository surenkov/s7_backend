
from rest_framework import mixins, generics
from rest_framework.views import Response
from rest_framework.generics import get_object_or_404

from . import serializers
from . import models


class GamerProfileView(mixins.CreateModelMixin,
                       mixins.RetrieveModelMixin,
                       generics.GenericAPIView):

    serializer_class = serializers.GamerProfileSerializer

    def get_queryset(self):
        return models.GamerProfile.objects.filter(user=self.request.user.pk)

    def get_object(self):
        return self.get_queryset().first()


class AllAchievementsView(generics.ListAPIView):
    serializer_class = serializers.AchievementSerializer
    queryset = models.Achievement.objects.all()


class MyAchievementsView(generics.ListAPIView):
    serializer_class = serializers.AchievementSerializer

    def get_queryset(self):
        return get_object_or_404(
            models.GamerProfile, user=self.request.user.pk).achievements.all()


class CountriesView(generics.ListAPIView):
    serializer_class = serializers.CountrySerializer
    queryset = models.Country.objects.all()


class CitiesView(generics.ListAPIView):
    serializers_class = serializers.CitySerializer

    def get_queryset(self):
        return models.City.objects.filter(
            country=self.request.data.get('country_id'))


class AirportsView(generics.ListAPIView):
    serializers_class = serializers.CitySerializer

    def get_queryset(self):
        return models.Airport.objects.filter(
            city=self.request.data.get('city_id'))


class AllAdventuresView(generics.ListAPIView):
    queryset = models.Adventure.objects.all()
    serializer_class = serializers.AdventureSerializer


class MyAdventuresView(generics.ListAPIView):
    serializer_class = serializers.UserAdventureSerializer

    def get_queryset(self):
        return get_object_or_404(models.GamerProfile,
                                 user=self.request.user.pk).adventures.all()


class AdventureTasksView(generics.RetrieveAPIView):
    serializer_class = serializers.AdventureProgressSerializer

    def get_queryset(self):
        return models.AdventureProgress.objects.filter(
            gamer=get_object_or_404(models.GamerProfile,
                                    user=self.request.user.pk)
        )

    def retrieve(self, request, pk=None, *args, **kwargs):
        progress = self.get_queryset().filter(adventure_id=pk)
        serializer = self.get_serializer(progress, many=True)
        return Response(serializer.data)


class ApplyToAdventureView(generics.CreateAPIView):
    serializer_class = serializers.AdventureProgressSerializer

    def create(self, request, pk=None, *args, **kwargs):
        adventure = models.Adventure.objects.get(pk=pk)
        serializer = self.get_serializer(data=dict(
            gamer=request.user.gamer,
            adventure=adventure,
            current_trip=adventure.start_trip,
        ))
        serializer.save()
        return Response(serializer.data)
