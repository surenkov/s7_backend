from rest_framework import fields
from rest_framework.generics import get_object_or_404
from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField
from .models import *


class AchievementSerializer(ModelSerializer):
    class Meta:
        model = Achievement
        fields = '__all__'


class TaskSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


class AdventureTaskSerializer(TaskSerializer):
    completed = fields.SerializerMethodField()

    def get_completed(self, obj):
        request, adventure = self.context['request'], self.context['adventure']
        return AdventureProgress.objects.filter(
            gamer=get_object_or_404(GamerProfile, user=request.user.pk),
            adventure=adventure,
            task=obj.pk
        ).exists()


class AdventureSerializer(ModelSerializer):
    achievement = AchievementSerializer()
    tasks = TaskSerializer(many=True)

    class Meta:
        model = Adventure
        fields = '__all__'


class UserAdventureSerializer(AdventureSerializer):
    tasks = fields.SerializerMethodField()

    def get_tasks(self, obj):
        context = {**self.context, 'adventure': obj}
        return AdventureTaskSerializer(obj.tasks.all().order_by('id'),
                                       context=context, many=True).data


class CountrySerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class CitySerializer(ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = City
        fields = '__all__'


class AirportSerializer(ModelSerializer):
    city = CitySerializer()

    class Meta:
        model = Airport
        fields = '__all__'


class AircraftSerializer(ModelSerializer):
    class Meta:
        model = Aircraft
        fields = '__all__'


class AchievementLogSerializer(ModelSerializer):
    gamer = PrimaryKeyRelatedField(queryset=GamerProfile.objects.all())
    achievement = AchievementSerializer()

    class Meta:
        model = AchievementLog
        exclude = ('task',)


class AdventureProgressSerializer(ModelSerializer):
    gamer = PrimaryKeyRelatedField(queryset=GamerProfile.objects.all())
    adventure = AdventureSerializer()
    task = TaskSerializer()

    class Meta:
        model = AdventureProgress
        fields = '__all__'


class GamerProfileSerializer(ModelSerializer):
    full_name = fields.CharField(source='user.get_full_name')
    email = fields.EmailField(source='user.email')
    achievements = fields.SerializerMethodField()

    class Meta:
        model = GamerProfile
        fields = (
            'full_name',
            'email',
            'achievements',
            'adventures',
            'trips',
            'score',
        )

    def get_adventures(self, obj):
        adventures = [p.adeventure for p in AdventureProgress.objects
                      .filter(gamer=obj).select_related('adventure')]
        return AdventureSerializer(adventures, many=True).data

    def get_achievements(self, obj):
        return AchievementLogSerializer(
            AchievementLog.objects.filter(gamer=obj),
            many=True,
        ).data
