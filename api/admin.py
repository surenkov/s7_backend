from django.contrib import admin
from .models import (
    AdventureProgress,
    Achievement,
    AchievementLog,
    Airport,
    Adventure,
    Task,
    GamerProfile,
    ScoreLog,
    City, Country)

admin.site.register(AdventureProgress)
admin.site.register(Achievement)
admin.site.register(AchievementLog)
admin.site.register(Airport)
admin.site.register(Adventure)
admin.site.register(Task)
admin.site.register(City)
admin.site.register(Country)
admin.site.register(GamerProfile)
admin.site.register(ScoreLog)
