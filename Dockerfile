FROM python:3.6-onbuild

ENV DJANGO_SETTINGS_MODULE=s7_backend.settings
ENV CELERY_CONFIG=s7_backend.celery

ENV PYTHONUNBUFFERED=1

ADD . /usr/src/app
